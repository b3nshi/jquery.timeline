/* jquery.timeline - v1.10.2.0 - 2013-07-22
* http://www.benjaminsimon.com.ar
* Copyright (c) 2013 Benjamín Simón; */
(function($){
    $.fn.timeline = function(opts){
        var defaults = {tAlign: false, typeEffect: 'down'};
        $.extend(defaults,opts);
        var obj = $(this);
        //First hide all items elements
        var items = obj.find('.item');
        items.hide();
        var w = obj.width();
        var h = obj.height();
        var name = obj.attr('id');
        
        var title = $('<div>'+obj.attr('title')+'</div>');
        
        var line = $('<span id="line_'+name+'" class="timeline"></span>');
        
        if (defaults.tAlign){
            title.css('float','left');
            line.css('float','left');
        }
        
        //Set params of line
        line.width(w*0.9);
        line.css('margin-left',w*0.05);
        line.css('margin-top',h*0.2);
        
        //Insert timeline into the component
        obj.prepend(line);
        obj.prepend(title);

        //Now I need insert every important point
        var num = items.size();
        var separation = Math.round(w*0.9/num);
        var i=0;
        var names = [];
        items.each(function(){
            var text = $(this).attr('id');
            var point = $('<span class="dot">'+text+'</span>');
            if ((i+1)==num){ 
                point.css('float','right');
            }else
                point.css('margin-right',separation-3);
            point.css('text-indent',text.length*-4);
            i=i+1;
            line.append(point);
        });
        
        if (num > 0){
            obj.find(".dot").click(function(){
                var clickObj = $(this);
                var list = clickObj.parent().parent();
                
                if (defaults.typeEffect == 'show')
                    list.find('.item').fadeOut(200);
                else
                    list.find('.item').slideUp(200);

                var bClose = $('<div class="dotclose">X</div>');
                
                if (defaults.typeEffect == 'show')
                    list.find('.item_'+clickObj.html()).fadeIn(800).prepend(bClose);
                else
                    list.find('.item_'+clickObj.html()).slideDown(800).append(bClose);

                obj.find(".dotclose").click(function(){
                    if (defaults.typeEffect == 'show')
                        $(this).parent().fadeOut(200);
                    else
                        $(this).parent().slideUp(200);
                });
            });
            }
    };
})(jQuery );
